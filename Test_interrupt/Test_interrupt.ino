const byte interruptPin = 25;
volatile int interruptCounter = 0;
int numberOfInterrupts = 0;

portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;


volatile uint32_t last_ISR = 0;
volatile uint32_t last_complete_ppm_interval = 0;
volatile uint32_t complete_ppm_interval = 0;
volatile uint8_t counter = 0;
volatile uint8_t max_counter = 0;
volatile bool new_ppm = false;;
volatile uint32_t raw_val[20];
uint32_t avg_val[20];



void IRAM_ATTR handleInterrupt() {
  portENTER_CRITICAL_ISR(&mux);

  unsigned long ISR_now = micros();
  unsigned long ISR_interval = ISR_now - last_ISR;
  last_ISR = ISR_now;

  if (ISR_interval > 2500) {//interval more than 2.5ms reset counter
    counter = 0;
    complete_ppm_interval = ISR_now - last_complete_ppm_interval;
    last_complete_ppm_interval = ISR_now;
    new_ppm = true;
  } else {
    raw_val[counter] = (uint16_t)ISR_interval;
    counter++;
    if (max_counter < counter)max_counter = counter;
  }

  portEXIT_CRITICAL_ISR(&mux);
}

void setup() {

  Serial.begin(115200);
  Serial.println("Monitoring PPM: ");
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, FALLING);

}

void loop() {

  if (new_ppm) {
    portENTER_CRITICAL(&mux);
    for (uint8_t i = 0; i < max_counter; i++) {
      avg_val[i] = (avg_val[i] + raw_val[i]) / 2;
    }
    portEXIT_CRITICAL(&mux);
    new_ppm = false;

    //    static uint32_t serial_interval = 0;
    //    if (millis() - serial_interval > 50) {
    //      serial_interval = millis();

    //    Serial.printf(" %d,", complete_ppm_interval);
    for (uint8_t i = 0; i < max_counter; i++) {
      Serial.printf(" %d,", avg_val[i]);
    }
    Serial.printf("\n");

    //    }
  }


}
