#include "MPU9250.h"
#include <MadgwickAHRS.h>

Madgwick filter;
// an MPU9250 object with the MPU-9250 sensor on SPI bus 0 and chip select pin 10
MPU9250FIFO IMU(SPI, 2);
int status;

// variables to hold FIFO data, these need to be large enough to hold the data
float a[3][26];
float g[3][26];
size_t fifoSize;
int dataFreqRec;
int procFreqRec;
int dataFreq;
int procFreq;
uint32_t start;
uint32_t stopWatch;

float avgAccRaw[3];
float avgAcc[3];
float calAcc[3];
bool isAccCal = true;

float avgGyRaw[3];
float avgGy[3];
float avgGyDeg[3];
float calGy[3];
bool isGyCal = true;

void setup() {
  // serial to display data
  Serial.begin(115200);
  while (!Serial) {}

  // start communication with IMU
  status = IMU.begin();
  if (status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(status);
    while (1) {}
  }
  // setting the accelerometer full scale range to +/-8G
  IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
  // setting DLPF bandwidth to 20 Hz
  IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
  // setting SRD to 19 for a 50 Hz update rate
  IMU.setSrd(0);
  // enabling the FIFO to record just the accelerometers
  IMU.enableFifo(true, true, false, false);

  filter.begin(500);
}

void loop() {


  static uint32_t time_loop = 0;
  if (millis() >= time_loop) {
    time_loop = millis() + 2;

    start = micros();
    IMU.readFifo();

    // get the X, Y, and Z accelerometer data and their size
    IMU.getFifoAccelY_mss(&fifoSize, a[0]);
    IMU.getFifoAccelX_mss(&fifoSize, a[1]);
    IMU.getFifoAccelZ_mss(&fifoSize, a[2]);
    IMU.getFifoGyroY_rads(&fifoSize, g[0]);
    IMU.getFifoGyroX_rads(&fifoSize, g[1]);
    IMU.getFifoGyroZ_rads(&fifoSize, g[2]);

    for (size_t i = 0; i < fifoSize; i++) {
      dataFreqRec ++;
    }

    for (size_t n = 0; n < 3; n++) {
      avgAccRaw[n] = 0.0;
      for (size_t i = 0; i < fifoSize; i++) {
        avgAccRaw[n] += a[n][i];
      }
      avgAccRaw[n] /= fifoSize;
      avgAcc[n] = avgAccRaw[n];// - calAcc[n];
      isAccCal = false;

      avgGyRaw[n] = 0.0;
      for (size_t i = 0; i < fifoSize; i++) {
        avgGyRaw[n] += g[n][i];
      }
      avgGyRaw[n] /= fifoSize;
      avgGy[n] = avgGyRaw[n] - calGy[n];
      if (isGyCal) {
        calGy[n] = calGy[n] * 0.995 + avgGyRaw[n] * 0.005;
        float minErr = 0.1;
        if (abs(avgGy[0]) <= minErr && abs(avgGy[1]) <= minErr && abs(avgGy[2]) <= minErr)
          isGyCal = false;
      }
    }

    if (!isAccCal && !isGyCal) {
      // update the filter, which computes orientation
      avgGyDeg[0] = avgGy[0] * RAD_TO_DEG;
      avgGyDeg[1] = avgGy[1] * RAD_TO_DEG;
      avgGyDeg[2] = avgGy[2] * RAD_TO_DEG;
      filter.updateIMU(-avgGyDeg[0], avgGyDeg[1], avgGyDeg[2] ,
                       avgAcc[0],   -avgAcc[1],  -avgAcc[2]);
      procFreqRec++;
    }
    stopWatch = micros() - start;
  }

  static uint32_t time_loop1 = 0;
  if (millis() >= time_loop1) {
    time_loop1 = millis() + 100;

    // print the data
    //    Serial.printf("The FIFO buffer is %d , dataFreq %d , procFreq %d ", fifoSize, dataFreq, procFreq);
    //    Serial.printf(" readFifo:%d us  ", stopWatch);
    //        Serial.printf("accZ %.4f\t%.4f\t%.4f\t\n", avgAcc[2], avgAcc[2], avgAcc[2]);
    float roll = filter.getRoll();
    float pitch = filter.getPitch();
    float yaw = filter.getYaw();
    //    Serial.printf("roll:%.4f pitch:%.4f Yaw:%.4f\n", roll , pitch, yaw);
        Serial.print("Orientation: ");
        Serial.print(yaw);
        Serial.print(" ");
        Serial.print(pitch);
        Serial.print(" ");
        Serial.println(roll);
    //    Serial.print("\tgyro: ");
    //    Serial.print(avgGyDeg[0]);
    //    Serial.print(" ");
    //    Serial.print(avgGyDeg[1]);
    //    Serial.print(" ");
    //    Serial.print(avgGyDeg[2]);
    //    Serial.print("\tAcc: ");
    //    Serial.print(avgAcc[0]);
    //    Serial.print(" ");
    //    Serial.print(avgAcc[1]);
    //    Serial.print(" ");
    //    Serial.println(avgAcc[2]);

//    Serial.printf("Orientation: %7.4f  %7.4f  %7.4f  ", yaw, pitch, roll);
//    Serial.printf("gyro: %7.4f  %7.4f  %7.4f  ", avgGyDeg[0], avgGyDeg[1], avgGyDeg[2]);
//    Serial.printf("acc: %7.4f  %7.4f  %7.4f  \n", avgAcc[0], avgAcc[1], avgAcc[2]);
  }

  static uint32_t time_loop2 = 0;
  if (millis() >= time_loop2) {
    time_loop2 = millis() + 1000;
    dataFreq = dataFreqRec;
    procFreq = procFreqRec;
    dataFreqRec = 0;
    procFreqRec = 0;
  }
}
