#ifdef ENABLE_OLED
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void Task_4(void *pvParameters) { // "Task OLED display"
  (void) pvParameters;

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" OLED");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );

  xSemaphoreTake( xMutex_i2c, portMAX_DELAY );
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    xSemaphoreTake( xMutex_serial, portMAX_DELAY );
    Serial.println(F("SSD1306 allocation failed"));
    xSemaphoreGive( xMutex_serial );
    xSemaphoreGive( xMutex_i2c );
    for (;;) {
      vTaskDelete( NULL );
    }
  }
  display.display();
  xSemaphoreGive( xMutex_i2c );
  vTaskDelay(2000); // Pause for 2 seconds

  xSemaphoreTake( xMutex_i2c, portMAX_DELAY );
  display.clearDisplay();
  display.display();
  xSemaphoreGive( xMutex_i2c );
  vTaskDelay(2000);

  for (;;) {
    xSemaphoreTake( xMutex_i2c, portMAX_DELAY );
    oled_display_bpm180();
    xSemaphoreGive( xMutex_i2c );
    vTaskDelay(500);

    if (OTA_UPLOAD_RUN == true) {
      xSemaphoreTake( xMutex_i2c, portMAX_DELAY );
      oled_display_OTA_update();
      xSemaphoreGive( xMutex_i2c );
      vTaskDelete( NULL );
    }
  }
}


void oled_display_bpm180() {
  display.clearDisplay();

  display.setTextSize(1);             // Normal 1:1 pixel scale

  display.setTextColor(SSD1306_BLACK, SSD1306_WHITE); // Draw 'inverse' text
  display.setCursor(0, 0);            // Start at top-left corner
  display.println(F("BMP"));

  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.print(F("Temp:"));
  display.println(temp, 1);
  display.print(F("pressure:"));
  display.println(pressure);
  display.print(F("alt:"));
  display.println(alt);

  display.display();
}

void oled_display_OTA_update() {
  display.clearDisplay();

  display.setTextSize(2);             // Normal 1:1 pixel scale

  display.setTextColor(SSD1306_BLACK, SSD1306_WHITE); // Draw 'inverse' text
  display.setCursor(30, 0);            // Start at top-left corner
  display.println(F("UPDATE"));

  display.display();
}
#endif
