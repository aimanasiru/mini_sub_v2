

void Task_11(void *pvParameters) { // "Task SerialRead"
  (void) pvParameters;

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" TelnetSerial");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );

  while (WiFi.status() != WL_CONNECTED) {
    vTaskDelay(1000);
  }
  server2.begin();
  server2.setNoDelay(true);

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print("Ready! Use 'telnet ");
  Serial.print(WiFi.localIP());
  Serial.println(" 23' to connect");
  xSemaphoreGive( xMutex_serial );

  for (;;) {

    xSemaphoreTake( xMutex_telnet, portMAX_DELAY );
    uint8_t i;
    if (WiFi.status() == WL_CONNECTED) {

      //check if there are any new clients
      if (server2.hasClient()) {
        for (i = 0; i < MAX_SRV_CLIENTS; i++) {
          //find free/disconnected spot
          if (!serverClients[i] || !serverClients[i].connected()) {
            if (serverClients[i]) serverClients[i].stop();
            serverClients[i] = server2.available();
            xSemaphoreTake( xMutex_serial, portMAX_DELAY );
            if (!serverClients[i]) Serial.println("available broken");
            Serial.print("New client: ");
            Serial.print(i); Serial.print(' ');
            Serial.println(serverClients[i].remoteIP());
            xSemaphoreGive( xMutex_serial );
            break;
          }
        }
        if (i >= MAX_SRV_CLIENTS) {
          //no free/disconnected spot so reject
          server2.available().stop();
        }
      }


      //check clients for data
      for (i = 0; i < MAX_SRV_CLIENTS; i++) {
        if (serverClients[i] && serverClients[i].connected()) {
          if (serverClients[i].available()) {
            //get data from the telnet client and push it to the UART

            //xSemaphoreTake( xMutex_serial, portMAX_DELAY );
            //while (serverClients[i].available()) Serial.write(serverClients[i].read());
            //xSemaphoreGive( xMutex_serial );

            char sbuf[20];
            uint8_t len = 0;
            while (serverClients[i].available() > 0) {
              sbuf[len] = serverClients[i].read();
              len++;
              vTaskDelay(1);
            }
            sbuf[len] = '\0';
            xSemaphoreTake( xMutex_serial, portMAX_DELAY );
            Serial.printf(sbuf);
            xSemaphoreGive( xMutex_serial );
            xSemaphoreGive( xMutex_telnet );
            readInputCommand(sbuf, strlen(sbuf));
            xSemaphoreTake( xMutex_telnet, portMAX_DELAY );
          }
        }
        else {
          if (serverClients[i]) {
            serverClients[i].stop();
          }
        }
      }


      //check UART for data
      //      if (Serial.available()) {
      //        size_t len = Serial.available();
      //        uint8_t sbuf[len];
      //        Serial.readBytes(sbuf, len);
      //        //push UART data to all connected telnet clients
      //        for (i = 0; i < MAX_SRV_CLIENTS; i++) {
      //          if (serverClients[i] && serverClients[i].connected()) {
      //            serverClients[i].write(sbuf, len);
      //            vTaskDelay(1);
      //          }
      //        }
      //      }
    }
    else {
      //      Serial.println("WiFi not connected!");
      for (i = 0; i < MAX_SRV_CLIENTS; i++) {
        if (serverClients[i]) serverClients[i].stop();
      }
    }
    xSemaphoreGive( xMutex_telnet );
    vTaskDelay(10);
    if (OTA_UPLOAD_RUN == true) {
      vTaskDelete( NULL );
    }
  }
}


void printTelnet(char* msg, size_t len) {
  xSemaphoreTake( xMutex_telnet, portMAX_DELAY );
  for (uint8_t i = 0; i < MAX_SRV_CLIENTS; i++) {
    if (serverClients[i] && serverClients[i].connected()) {
      serverClients[i].write(msg, len);
      vTaskDelay(1);
    }
  }
  xSemaphoreGive( xMutex_telnet );
}
