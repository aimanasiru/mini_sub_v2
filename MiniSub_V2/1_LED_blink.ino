
#define LED_WIFI_NOT_CONNECTED 1

class ledState {
  public:
    bool isWifiConnected = false;
    bool isBMP180Ready = false;
    bool isOledReady = false;
    bool isMArm = false;
    uint8_t isWifiConnected_blink = 2;
    uint8_t isBMP180Ready_blink = 3;
    uint8_t isOledReady_blink = 4;
    uint8_t isMArm_blink = 5;
};
volatile ledState LED;


void Task_1(void *pvParameters) { // "Task Blink LED"

  (void) pvParameters;
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" Blink");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );

  const int LED_BUILTIN = 2;
  pinMode(LED_BUILTIN, OUTPUT);
  //  const int ledChannel = 0;
  //  ledcSetup(ledChannel, 50, 16);
  //  ledcAttachPin(LED_BUILTIN, 0);

  for (;;) {
    //    digitalWrite(LED_BUILTIN, HIGH);
    //    vTaskDelay(50);
    //    digitalWrite(LED_BUILTIN, LOW);
    //    vTaskDelay(950);

    // increase the LED brightness
    //    for (int percent = 0; percent <= 100; percent += 2) {
    //
    //      ledcWrite(ledChannel, _percentToDuty(percent));
    //      vTaskDelay(20);
    //    }
    //
    //    // decrease the LED brightness
    //    for (int percent = 100; percent >= 0; percent -= 2) {
    //      ledcWrite(ledChannel, _percentToDuty(percent));
    //      vTaskDelay(20);
    //    }

    const int delayON = 5;
    const int delayOFF = 100;
    const int delayBetween = 800;

    //blink or wifi not connected
    if (LED.isWifiConnected == false) {
      for (uint8_t i = 0; i < LED.isWifiConnected_blink ; i++) {
        digitalWrite(LED_BUILTIN, HIGH);
        vTaskDelay(delayON);
        digitalWrite(LED_BUILTIN, LOW);
        vTaskDelay(delayOFF);
      }
    }
    vTaskDelay(delayBetween);

    //blink or wifi not connected
    if (LED.isBMP180Ready == false) {
      for (uint8_t i = 0; i < LED.isBMP180Ready_blink ; i++) {
        digitalWrite(LED_BUILTIN, HIGH);
        vTaskDelay(delayON);
        digitalWrite(LED_BUILTIN, LOW);
        vTaskDelay(delayOFF);
      }
    }
    vTaskDelay(delayBetween);

    //blink or wifi not connected
    if (LED.isOledReady == false) {
      for (uint8_t i = 0; i < LED.isOledReady_blink ; i++) {
        digitalWrite(LED_BUILTIN, HIGH);
        vTaskDelay(delayON);
        digitalWrite(LED_BUILTIN, LOW);
        vTaskDelay(delayOFF);
      }
    }
    vTaskDelay(delayBetween);

    //blink or wifi not connected
    if (LED.isMArm == false) {
      for (uint8_t i = 0; i < LED.isMArm_blink ; i++) {
        digitalWrite(LED_BUILTIN, HIGH);
        vTaskDelay(delayON);
        digitalWrite(LED_BUILTIN, LOW);
        vTaskDelay(delayOFF);
      }
    }
    vTaskDelay(delayBetween);

  }
}


int _percentToDuty(int _percent)    {
  return map(_percent, 0, 100, 0, 65535);
}
