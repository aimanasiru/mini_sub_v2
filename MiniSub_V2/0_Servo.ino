


void Task_8(void *pvParameters) { // "Task Blink LED"
  (void) pvParameters;

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" Servo");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );


  static const int PWM_freq = 50;
  static const int PWM_bit = 16;

  //  ledcSetup(ledChannel, freq, resolution);
  for (int i = 1; i <= 7; i++)
    ledcSetup(i, PWM_freq, PWM_bit);

  ledcAttachPin(13, 1);
  ledcAttachPin(12, 2);
  ledcAttachPin(14, 3);
  ledcAttachPin(27, 4);
  ledcAttachPin(26, 5);
  ledcAttachPin(25, 6);
  ledcAttachPin(33, 7);


  int pos = 0;
  
  TickType_t xLastWakeTime;
  const TickType_t xElapse = 10;//100Hz Loop
  xLastWakeTime = xTaskGetTickCount();
  for (;;) {
    // Wait for the next cycle.
    vTaskDelayUntil( &xLastWakeTime, xElapse );
    
    if (servoSweep_test) {
      for (pos = 1000; pos <= 2000; pos += 10) { // goes from 0 degrees to 180 degrees
        // in steps of 1 degree
        vTaskDelay(10);
        ledcWrite(1, _usToDuty(pos));
        ledcWrite(2, _usToDuty(pos));
        ledcWrite(3, _usToDuty(pos));
        ledcWrite(4, _usToDuty(pos));
        ledcWrite(5, _usToDuty(pos));
        ledcWrite(6, _usToDuty(pos));
        ledcWrite(7, _usToDuty(pos));
      }
      vTaskDelay(500);
      for (pos = 2000; pos >= 1000; pos -= 10) { // goes from 180 degrees to 0 degrees
        vTaskDelay(10);
        ledcWrite(1, _usToDuty(pos));
        ledcWrite(2, _usToDuty(pos));
        ledcWrite(3, _usToDuty(pos));
        ledcWrite(4, _usToDuty(pos));
        ledcWrite(5, _usToDuty(pos));
        ledcWrite(6, _usToDuty(pos));
        ledcWrite(7, _usToDuty(pos));
      }
      vTaskDelay(500);


    } else if (directChannel_test) {
      //test every servo direct from receiver chanel
      ledcWrite(1, _usToDuty(Channel[0]));
      ledcWrite(2, _usToDuty(Channel[1]));
      ledcWrite(3, _usToDuty(Channel[2]));
      ledcWrite(5, _usToDuty(Channel[3]));
      ledcWrite(6, _usToDuty(Channel[4]));
      ledcWrite(7, _usToDuty(Channel[5]));
    } else {


      ledcWrite(1, _usToDuty(Servo_Out[0]));
      ledcWrite(2, _usToDuty(Servo_Out[1]));
      ledcWrite(3, _usToDuty(Servo_Out[2]));
      ledcWrite(4, _usToDuty(Servo_Out[3]));
      ledcWrite(5, _usToDuty(Servo_Out[4]));
      ledcWrite(6, _usToDuty(Servo_Out[5]));
      ledcWrite(7, _usToDuty(Servo_Out[6]));
    }

    if (debug_motor) {
      xSemaphoreTake( xMutex_serial, portMAX_DELAY );
      for (uint8_t i = 0; i < Total_Motor; i++) {
        Serial.printf(" %d,", Servo_Out[i]);
      }
      Serial.printf("\n");
      xSemaphoreGive( xMutex_serial );
    }

    if (OTA_UPLOAD_RUN == true) {
      vTaskDelete( NULL );
    }
  }
}

const int MAX_COMPARE = ((1 << 16) - 1); // 65535
const int TAU_MSEC = 20;
const int TAU_USEC = (TAU_MSEC * 1000);

int _usToDuty(int us)    {
  return map(us, 0, TAU_USEC, 0, MAX_COMPARE);
}
