/*
   Server Index Page
*/

const char* serverIndex =
  "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
  "<form method='POST' action='#' enctype='multipart/form-data' id='upload_form'>"
  "<input type='file' name='update'>"
  "<input type='submit' value='Update'>"
  "</form>"
  "<div id='prg'>progress: 0%</div>"
  "<script>"
  "$('form').submit(function(e){"
  "e.preventDefault();"
  "var form = $('#upload_form')[0];"
  "var data = new FormData(form);"
  " $.ajax({"
  "url: '/update',"
  "type: 'POST',"
  "data: data,"
  "contentType: false,"
  "processData:false,"
  "xhr: function() {"
  "var xhr = new window.XMLHttpRequest();"
  "xhr.upload.addEventListener('progress', function(evt) {"
  "if (evt.lengthComputable) {"
  "var per = evt.loaded / evt.total;"
  "$('#prg').html('progress: ' + Math.round(per*100) + '%');"
  "}"
  "}, false);"
  "return xhr;"
  "},"
  "success:function(d, s) {"
  "console.log('success!')"
  "},"
  "error: function (a, b, c) {"
  "}"
  "});"
  "});"
  "</script>";



void OTA_initialization() {
  /*use mdns for host name resolution*/
  if (!MDNS.begin(host)) { //http://esp32.local
    xSemaphoreTake( xMutex_serial, portMAX_DELAY );
    Serial.println("Error setting up MDNS responder!");
    xSemaphoreGive( xMutex_serial );
    while (1) {
      vTaskDelay(10);//delay(1000);
    }
  }
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println("mDNS responder started");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(10);


  /*return index page which is stored in serverIndex */
  //  server.on("/", HTTP_GET, []() {
  //    server.sendHeader("Connection", "close");
  //    server.send(200, "text/html", loginIndex);
  //  });
  //  server.on("/serverIndex", HTTP_GET, []() {
  //    server.sendHeader("Connection", "close");
  //    server.send(200, "text/html", serverIndex);
  //  });
  server.on("/", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", serverIndex);
  });



  /*handling uploading firmware file */
  server.on("/update", HTTP_POST, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    ESP.restart();
  }, []() {
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      vTaskDelay(1);
      OTA_UPLOAD_RUN = true;
      Serial.printf("Updating: %s\n", upload.filename.c_str());
      if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      vTaskDelay(1);
      /* flashing firmware to ESP*/
      if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      vTaskDelay(1);
      if (Update.end(true)) { //true to set the size to the current progress
        Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        Update.printError(Serial);
      }
    }
  });
  server.begin();
}


void Task_2(void *pvParameters)  //"Task_OTA_update"
{
  (void) pvParameters;
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" OtaUpdate");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );

  WiFi.begin();
  WiFi.setTxPower(WIFI_POWER_19_5dBm);

  vTaskDelay(1000);
  if (WiFi.status() != WL_CONNECTED) {

    // Connect to WiFi network
    WiFi.disconnect();

    vTaskDelay(100);
    WiFi.begin(ssid, password);
    WiFi.setTxPower(WIFI_POWER_19_5dBm);

    xSemaphoreTake( xMutex_serial, portMAX_DELAY );
    Serial.println("");
    xSemaphoreGive( xMutex_serial );

    // Wait for connection
    unsigned long wifitimeout = millis() + 6000;
    while (WiFi.status() != WL_CONNECTED) {
      vTaskDelay(100);
      xSemaphoreTake( xMutex_serial, portMAX_DELAY );
      Serial.print(".");
      if (millis() > wifitimeout) {
        vTaskDelete( NULL );//disable ota.
      }
      xSemaphoreGive( xMutex_serial );
    }

  }

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  xSemaphoreGive( xMutex_serial );

  OTA_initialization();
  for (;;) {

    if (WiFi.status() != WL_CONNECTED) {
      vTaskDelay(1000);
      // Connect to WiFi network
      WiFi.disconnect();

      vTaskDelay(100);
      WiFi.begin(ssid, password);
      WiFi.setTxPower(WIFI_POWER_19_5dBm);

      vTaskDelay(1000);
    } else {
      server.handleClient();
    }

    vTaskDelay(100);
  }
}
