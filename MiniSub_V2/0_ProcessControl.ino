
#define INTEG_MAX    200
#define INTEG_MIN    -200


void Task_10(void *pvParameters) { // "ProcessControl"
  (void) pvParameters;

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" ProcessControl");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );

  //========    PID gain and limit settings   ==========//
  const float Hz = 100;
  const int output_bits = 16;
  const bool output_signed = true;
  const int16_t servo_center = 1500;

  FastPID PID_rol(saved_data.Kp[0], saved_data.Kp[0], saved_data.Kp[0], Hz, output_bits, output_signed);
  FastPID PID_pit(saved_data.Kp[1], saved_data.Kp[1], saved_data.Kp[1], Hz, output_bits, output_signed);
  FastPID PID_yaw(saved_data.Kp[2], saved_data.Kp[2], saved_data.Kp[2], Hz, output_bits, output_signed);
  FastPID PID_hea(saved_data.Kp[3], saved_data.Kp[3], saved_data.Kp[3], Hz, output_bits, output_signed);

  PID_rol.setOutputRange(-500, 500);
  PID_pit.setOutputRange(-500, 500);
  PID_yaw.setOutputRange(-500, 500);
  PID_hea.setOutputRange(-500, 500);

  int loop_count = 0;
  uint32_t loop_time_1s = 0;

  TickType_t xLastWakeTime;
  const TickType_t xElapse = 10; // set loop to 100Hz
  xLastWakeTime = xTaskGetTickCount();

  for (;;) {

    // Wait for the next cycle.
    vTaskDelayUntil( &xLastWakeTime, xElapse );

    //=========== Check Arming Switch =============//
    static int stickState = 5;
    if (abs(Channel[4] - 1000) < 100 && stickState != 0) {
      stickState = 0;
      isArmed = false;
      isPID = true;
      sprintf(buffer, " isArmed : %s isPID : %s\n",
              isArmed ? "true" : "false",
              isPID ? "true" : "false");
      printBothSerialTelnet(buffer, strlen(buffer));
    } else if (abs(Channel[4] - 1500) < 100 && stickState != 1) {
      stickState = 1;
      isArmed = true;
      isPID = false;
      sprintf(buffer, " isArmed : %s isPID : %s\n",
              isArmed ? "true" : "false",
              isPID ? "true" : "false");
      printBothSerialTelnet(buffer, strlen(buffer));
    } else if (abs(Channel[4] - 2000) < 100 && stickState != 2) {
      stickState = 2;
      isArmed = true;
      isPID = true;
      sprintf(buffer, " isArmed : %s isPID : %s\n",
              isArmed ? "true" : "false",
              isPID ? "true" : "false");
      printBothSerialTelnet(buffer, strlen(buffer));
    }

    //=========== Get INPUT SETPOINT AND OUTPUT  =============//
    xSemaphoreTake( xMutex_imuData, portMAX_DELAY );
    //input range -500 to +500
    int16_t swy_setpoint = Channel[0] - servo_center;//== Input SWAY (left/right)
    int16_t sur_setpoint = Channel[1] - servo_center;//== Input SURGE (forward/backward)
    int16_t hea_setpoint = Channel[2] - servo_center;//== Input HEAVE (up/down)
    int16_t rol_setpoint = 0;
    int16_t pit_setpoint = 0;
    int16_t yaw_setpoint = Channel[3] - servo_center;//== Input Yaw (clockwise/anticlockwise)

    // YAW Calculation
    static int16_t yaw_curr, yaw_target, yaw_error, newtarget;
    yaw_curr = (int16_t)YAW;
    if (abs(Channel[3] - 1500) > 100) {
      //      newtarget = yaw_curr + constrain(((Channel[3] - servo_center) / 10), -45, 45) ;
      yaw_error = constrain(((Channel[3] - servo_center) / 10), -100, 100);
      yaw_target = yaw_curr;

    } else {
      yaw_error = circleAroundHalf(yaw_curr - yaw_target);
    }

    if ( debug_yaw) {
      static uint8_t count = 0;
      if (++count > 2) {
        count = 0;
        sprintf(buffer, "target,%d,curr,%d,erro,%d, newTar, %d\n",
                yaw_target, yaw_curr, yaw_error, newtarget
               );
        printBothSerialTelnet(buffer, strlen(buffer));
      }
    }


    //Heave Calculation
    static int16_t hea_curr, hea_target, hea_error, heanewtarget;
    hea_curr = (int16_t)(filteredPressure / 10);
    if (abs(Channel[2] - servo_center) > 200) {
      hea_error = constrain(((Channel[2] - servo_center) / 100) * 5, -20, 20);
      hea_target = hea_curr;

    } else {
      hea_error = hea_curr - hea_target;
    }

    if ( debug_hea) {
      static uint8_t count = 0;
      if (++count > 2) {
        count = 0;
        sprintf(buffer, "target,%d,curr,%d,erro,%d, newTar, %d\n",
                hea_target, hea_curr, hea_error, heanewtarget
               );
        printBothSerialTelnet(buffer, strlen(buffer));
      }
    }

    int16_t swy_input = swy_setpoint;
    int16_t sur_input = sur_setpoint;
    int16_t hea_input = hea_setpoint;
    int16_t rol_input = (int16_t) - ROLL;
    int16_t pit_input = (int16_t)PITCH;
    int16_t yaw_input = yaw_setpoint;
    xSemaphoreGive( xMutex_imuData );

    int16_t swy = swy_input;
    int16_t sur = sur_input;
    int16_t hea = 0;
    int16_t rol = 0;
    int16_t pit = 0;
    int16_t yaw = 0;

    if (isPID) {

      if (newPID) {
        newPID = false;
        PID_rol.setCoefficients(saved_data.Kp[0], saved_data.Ki[0], saved_data.Kd[0], Hz);
        PID_pit.setCoefficients(saved_data.Kp[1], saved_data.Ki[1], saved_data.Kd[1], Hz);
        PID_yaw.setCoefficients(saved_data.Kp[2], saved_data.Ki[2], saved_data.Kd[2], Hz);
        PID_hea.setCoefficients(saved_data.Kp[3], saved_data.Ki[3], saved_data.Kd[3], Hz);
      }

      rol = PID_rol.step(rol_setpoint, rol_input);
      pit = PID_pit.step(pit_setpoint, pit_input);
      hea = PID_hea.step(0, hea_error) * -1;

      //For YAW ONLY CAUSE IT WILL CIRCLE ARROUND
      if (abs(yaw_error) < 1 ) {
        PID_yaw.clear();
      } else {
        yaw = PID_yaw.step(0, yaw_error);
      }
    } else {
      PID_rol.clear();
      PID_pit.clear();
      PID_yaw.clear();
      PID_hea.clear();

      hea = hea_input;
      yaw = -yaw_input;
    }

    //========  Integrate movement to motor (kinematic)  ========//
    //motor_val range -500 to +500
    int16_t motor_val[Total_Motor];
    motor_val[0] = rol + hea - pit;           // motor 1 front left
    motor_val[1] =  yaw + swy;                // motor 2 front center
    motor_val[2] = -rol + hea - pit;          // motor 3 front right
    motor_val[4] = sur - (swy * 0.8) + yaw;   //Motor 5 back left
    motor_val[5] = hea + pit;                 //motor 6 back center
    motor_val[6] = -yaw + sur + (swy * 0.8);        //motor 7 back right


    //========  normalize all motor value  ========//
    //get highest value
    int16_t highest_val = 0;
    for (int i = 0; i < Total_Motor; i++) {
      if (highest_val < abs(motor_val[i]))
        highest_val = abs(motor_val[i]);
    }

    //normalize all value if out of range
    if (highest_val > 500) {
      float mult = 500 / (float)highest_val ;
      for (int i = 0; i < Total_Motor; i++) {
        float tempVal = (float)motor_val[i] * mult;
        motor_val[i] = (int16_t)tempVal;
      }
    }

    //========  Set pump value  ========//
    if (abs(Channel[5] - 1000) < 50) {//channel 6 low
      motor_val[3] = 0;
    } else if (abs(Channel[5] - 2000) < 50) {//channel high
      motor_val[3] = 450;
    } else {
      motor_val[3] = 0;
    }

    //========  Assign all motor value  ========//
    //Servo_Out range 1000 to 2000
    // set output value to servo
    for (uint8_t i = 0; i < Total_Motor; i++) {
      if (isArmed)
        Servo_Out[i] = servo_center + motor_val[i] ;
      else {//diable all motor if disarm
        Servo_Out[i] = servo_center;
      }
    }

    if (debug_pid_rpy) {
      static int count = 0;
      if (count == 0)
        sprintf(buffer, "rol,%d,%d,%d,", rol_input, rol_setpoint, rol);
      else if (count == 1)
        sprintf(buffer, "pit,%d,%d,%d,", pit_input, pit_setpoint, pit);
      else if (count == 2)
        sprintf(buffer, "yaw,%d,%d,%d,", (int16_t)YAW, yaw_target, yaw);
      else if (count == 3)
        sprintf(buffer, "pres,%d\n", filteredPressure);

      if (++count > 3)count = 0;
      printBothSerialTelnet(buffer, strlen(buffer));

    }

    if (processFreq_test) {
      loop_count++;
      if (millis() >= loop_time_1s) {
        loop_time_1s = millis() + 1000;
        sprintf(buffer, " processFreq = %d Hz \n", loop_count);
        printBothSerialTelnet(buffer, strlen(buffer));
        loop_count = 0;
      }
    }

    if (debug_process_control) {
      sprintf(buffer, " %d,%d,%d,%d,%d,%d,  ",
              swy, sur, hea,
              rol, pit, yaw);
      for (uint8_t i = 0; i < Total_Motor; i++) {
        char msg[10]; sprintf(msg, " %d,", motor_val[i]);
        strcat(buffer, msg);
      }
      strcat(buffer, "\n");
      printBothSerialTelnet(buffer, strlen(buffer));
    }

    if (OTA_UPLOAD_RUN == true) {
      vTaskDelete( NULL );
    }
  }
}

int16_t circleAround(int16_t input) {
  if (input > 360)
    return input - 360;
  if (input < 0 )
    return input + 360;
}
int16_t circleAroundHalf(int16_t input) {
  if (input > 180)
    return input - 360;
  if (input < -180 )
    return input + 360;
}
