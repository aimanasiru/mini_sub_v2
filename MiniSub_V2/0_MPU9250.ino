void Task_12(void *pvParameters) { // "Task SerialWrite"
  (void) pvParameters;

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" MPU0250");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );

  Madgwick AHRS;
  MPU9250FIFO IMU(SPI, 2);

  int status;

  // variables to hold FIFO data, these need to be large enough to hold the data
  float a[3][26];
  float g[3][26];
  size_t fifoSize;
  int dataFreqRec;
  int procFreqRec;
  int dataFreq;
  int procFreq;
  uint32_t start;
  uint32_t stopWatch;

  float avgAccRaw[3];
  float avgAcc[3];
  float calAcc[3];
  bool isAccCal = true;

  float avgGyRaw[3];
  float avgGy[3];
  float avgGyDeg[3];
  float calGy[3];
  bool isGyCal = true;

  // start communication with IMU
  status = IMU.begin();
  if (status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(status);
    sprintf(buffer, "IMU initialization unsuccessful\n");
    printBothSerialTelnet(buffer, strlen(buffer));
    vTaskDelete( NULL );
  }
  // setting the accelerometer full scale range to +/-8G
  IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
  // setting DLPF bandwidth to 20 Hz
  IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
  // setting SRD to 19 for a 50 Hz update rate
  IMU.setSrd(9); // 9 for 100hz // 0 for 1000Hz
  // enabling the FIFO to record just the accelerometers
  //  IMU.enableFifo(true, true, false, false);

  AHRS.begin(100);

  TickType_t xLastWakeTime;
  const TickType_t xElapse = 10;//100Hz loop
  xLastWakeTime = xTaskGetTickCount();

  for (;;) {

    // Wait for the next cycle.
    vTaskDelayUntil( &xLastWakeTime, xElapse );

    if (debug_mpu9250_0)start = micros();

    //    IMU.readFifo();

    // get the X, Y, and Z accelerometer data and their size
    //    IMU.getFifoAccelY_mss(&fifoSize, a[0]);
    //    IMU.getFifoAccelX_mss(&fifoSize, a[1]);
    //    IMU.getFifoAccelZ_mss(&fifoSize, a[2]);
    //    IMU.getFifoGyroY_rads(&fifoSize, g[0]);
    //    IMU.getFifoGyroX_rads(&fifoSize, g[1]);
    //    IMU.getFifoGyroZ_rads(&fifoSize, g[2]);
    IMU.readSensor();

    if (debug_mpu9250_0) {
      for (size_t i = 0; i < fifoSize; i++) {
        dataFreqRec ++;
      }
    }

    //    for (size_t n = 0; n < 3; n++) {
    //      avgAccRaw[n] = 0.0;
    //      for (size_t i = 0; i < fifoSize; i++) {
    //        avgAccRaw[n] += a[n][i];
    //      }
    //      avgAccRaw[n] /= fifoSize;
    //      avgAcc[n] = avgAccRaw[n];// - calAcc[n];
          isAccCal = false;
    //
    //      avgGyRaw[n] = 0.0;
    //      for (size_t i = 0; i < fifoSize; i++) {
    //        avgGyRaw[n] += g[n][i];
    //      }
    //      avgGyRaw[n] /= fifoSize;
    //      avgGy[n] = avgGyRaw[n] - calGy[n];
    //      if (isGyCal) {
    //        calGy[n] = calGy[n] * 0.995 + avgGyRaw[n] * 0.005;
    //        float minErr = 0.1;
    //        if (abs(avgGy[0]) <= minErr && abs(avgGy[1]) <= minErr && abs(avgGy[2]) <= minErr)
    //          isGyCal = false;
    //      }
    //    }

    avgAcc[0] = IMU.getAccelY_mss();
    avgAcc[1] = IMU.getAccelX_mss();
    avgAcc[2] = IMU.getAccelZ_mss();
    avgGyRaw[0] = IMU.getGyroY_rads();
    avgGyRaw[1] = IMU.getGyroX_rads();
    avgGyRaw[2] = IMU.getGyroZ_rads();



    for (size_t n = 0; n < 3; n++) {
      avgGy[n] = avgGyRaw[n] - calGy[n];
      if (isGyCal) {
        calGy[n] = calGy[n] * 0.995 + avgGyRaw[n] * 0.005;
        float minErr = 0.1;
        if (abs(avgGy[0]) <= minErr && abs(avgGy[1]) <= minErr && abs(avgGy[2]) <= minErr)
          isGyCal = false;
      }
    }

    if (debug_mpu9250_2) {
      static uint32_t time_loop0 = 0;
      if (millis() >= time_loop0) {
        time_loop0 = millis() + 100;
        sprintf(buffer, "accel,%.2f,%.2f,%.2f,gyro,%.2f,%.2f,%.2f\n",
                avgAcc[0], avgAcc[1], avgAcc[2],
                avgGy[0], avgGy[1], avgGy[2]);
        printBothSerialTelnet(buffer, strlen(buffer));
      }
    }

    if (!isAccCal && !isGyCal) {
      // update the filter, which computes orientation
      avgGyDeg[0] = avgGy[0] * RAD_TO_DEG;
      avgGyDeg[1] = avgGy[1] * RAD_TO_DEG;
      avgGyDeg[2] = avgGy[2] * RAD_TO_DEG;



      AHRS.updateIMU(-avgGyDeg[0], avgGyDeg[1], avgGyDeg[2] ,
                     avgAcc[0],   -avgAcc[1],  -avgAcc[2]);

      xSemaphoreTake( xMutex_imuData, portMAX_DELAY );
      ROLL = AHRS.getRoll();
      PITCH = AHRS.getPitch();
      YAW = AHRS.getYaw();
      xSemaphoreGive( xMutex_imuData );

      procFreqRec++;
    }

    if (debug_mpu9250_0) {
      static uint32_t time_loop1 = 0;
      if (millis() >= time_loop1) {
        time_loop1 = millis() + 1000;
        sprintf(buffer, "The FIFO buffer is %d, dataFreq:%d, procFreq:%d, timeUsed:%dus\n", fifoSize, dataFreq, procFreq, stopWatch);
        printBothSerialTelnet(buffer, strlen(buffer));
        dataFreq = dataFreqRec;
        procFreq = procFreqRec;
        dataFreqRec = 0;
        procFreqRec = 0;
      }
    }

    if (debug_mpu9250_1) {
      static uint32_t time_loop2 = 0;
      if (millis() >= time_loop2) {
        time_loop2 = millis() + 100;
        sprintf(buffer, "Orientation: %.2f %.2f %.2f ;\n", YAW, PITCH, ROLL);
        printBothSerialTelnet(buffer, strlen(buffer));
      }
    }

    if (debug_mpu9250_0)stopWatch = micros() - start;

    //    vTaskDelay(1);
    if (OTA_UPLOAD_RUN == true) {
      vTaskDelete( NULL );
    }
  }
}
