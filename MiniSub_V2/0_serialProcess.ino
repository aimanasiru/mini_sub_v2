void Task_6(void *pvParameters) { // "Task SerialRead"
  (void) pvParameters;

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" SerialRead");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );

  for (;;) {


    if (Serial.available()) {
      size_t len = Serial.available();
      char sbuf[len + 1];
      xSemaphoreTake( xMutex_serial, portMAX_DELAY );
      Serial.readBytes(sbuf, len); sbuf[len] = '\0';
      xSemaphoreGive( xMutex_serial );

      readInputCommand(sbuf, len);
    }
    vTaskDelay(10);

    if (OTA_UPLOAD_RUN == true) {
      vTaskDelete( NULL );
    }
  }
}


void readInputCommand(char* msg, size_t len) {

  if (strstr(msg, "ppm")) {
    debug_ppm = !debug_ppm;
    sprintf(buffer, " receiver debug : %s\n", debug_ppm ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "power")) {
    //toggle receiver debug
    //    isPower = !isPower;
    //    sprintf(buffer, " isPower : %s\n", isPower ? "true" : "false");
    //    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "direct")) {
    directChannel_test = !directChannel_test;
    sprintf(buffer, " directChannel_test debug : %s\n", directChannel_test ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "freq")) {
    processFreq_test = !processFreq_test;
    sprintf(buffer, " processFreq_test debug : %s\n", processFreq_test ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "motor")) {
    debug_motor = !debug_motor;
    sprintf(buffer, " debug_motor debug : %s\n", debug_motor ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "process")) {
    debug_process_control = !debug_process_control;
    sprintf(buffer, " debug_process_control debug : %s\n", debug_process_control ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));


  } else if (strstr(msg, "rpy")) {
    debug_pid_rpy = !debug_pid_rpy;
    sprintf(buffer, " debug_pid_rpy debug : %s\n", debug_pid_rpy ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "mpu0")) {
    debug_mpu9250_0 = !debug_mpu9250_0;
    sprintf(buffer, " debug_mpu9250_0 debug : %s\n", debug_mpu9250_0 ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "mpu1")) {
    debug_mpu9250_1 = !debug_mpu9250_1;
    sprintf(buffer, " debug_mpu9250_1 debug : %s\n", debug_mpu9250_1 ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "mpu2")) {
    debug_mpu9250_2 = !debug_mpu9250_2;
    sprintf(buffer, " debug_mpu9250_2 debug : %s\n", debug_mpu9250_2 ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "yaw")) {
    debug_yaw = !debug_yaw;
    sprintf(buffer, " debug_yaw debug : %s\n", debug_yaw ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "hea")) {
    debug_hea = !debug_hea;
    sprintf(buffer, " debug_hea debug : %s\n", debug_hea ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "cal_imu")) {
    calIMU = true;
    sprintf(buffer, " calIMU : start");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "bmp")) {
    debug_bmp = !debug_bmp;
    sprintf(buffer, " debug_bmp debug : %s\n", debug_bmp ? "true" : "false");
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "PID")) {
    // format PID,<1:save/2:view>,<pid selection>,Kp,Ki,Kd
    char *p;
    uint8_t i;
    char tbuf[40];
    float floatData[10];
    strcpy(buffer, "");
    for (i = 1, p = strtok(msg, ","); p != NULL; p = strtok(NULL, ","), i++) {
      sprintf(tbuf, "data_%u=%s\n", i, p);
      strcat(buffer, tbuf);
      if (i > 1)
        floatData[i] = (float)atof(p);
    }
    int cmd = (int)floatData[2];
    int n = (int)floatData[3];

    if (cmd == 1) { //save
      sprintf(tbuf, "Saving selection %d \n", n );
      strcat(buffer, tbuf);
      saved_data.Kp[n] = floatData[4];
      saved_data.Ki[n] = floatData[5];
      saved_data.Kd[n] = floatData[6];
      newPID = true;
      saveEEPROM();

      sprintf(tbuf, "selection=%d , data Kp=%.2f, Ki %.2f, Kd %.2f \n",
              n,
              saved_data.Kp[n],
              saved_data.Ki[n],
              saved_data.Kd[n]
             );
    } else { //check
      char tbuf[50];
      strcpy(buffer, "");
      for (uint8_t i = 0; i < 6; i++) {
        sprintf(tbuf, "PID%d %.6f, %.6f, %.6f,\n" , i,
                saved_data.Kp[i], saved_data.Ki[i], saved_data.Kd[i]);
        strcat(buffer, tbuf);
      }
    }
    strcat(buffer, tbuf);
    printBothSerialTelnet(buffer, strlen(buffer));

  } else if (strstr(msg, "help")) {
    const char help_msg[] =
      "\n\n"
      "============= HELP ============\n"
      " DEBUG MODE \n"
      " ppm     > debug_ppm \n"
      " direct  > directChannel_test \n"
      " freq    > processFreq_test \n"
      " motor   > debug_motor \n"
      " process > debug_process_control \n"
      " mpu0    > debug_mpu9250_0 \n"
      " mpu1    > debug_mpu9250_1 \n"
      "================================"
      "\n\n\0";
    sprintf(buffer, "%s", help_msg);
    printBothSerialTelnet(buffer, strlen(buffer));

  }
}

void printBothSerialTelnet(char* msg, size_t len) {
  printSerial(msg, len);
  vTaskDelay(2);
  printTelnet(msg, len);
}


void printSerial(char* msg, size_t len) {
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.printf(msg);
  xSemaphoreGive( xMutex_serial );
}
