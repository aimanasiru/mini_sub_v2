//NEED update: more stable temperature and pressure in same time at library
//#define debug_i2c_bpm180


float temp;
int32_t pressure;
int32_t filteredPressure;
float alt;
bool isReadyBmp180 = false;;


void Task_5(void *pvParameters) { // "Task BMP180"
  (void) pvParameters;

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" BMP180");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );

  xSemaphoreTake( xMutex_i2c, portMAX_DELAY );
  if (!bmp.begin()) {
    xSemaphoreTake( xMutex_serial, portMAX_DELAY );
    Serial.println("Could not find a valid BMP085 sensor, check wiring!");
    xSemaphoreGive( xMutex_serial );
    xSemaphoreGive( xMutex_i2c );
    for (;;) {
      vTaskDelete( NULL );
    }
  }
  xSemaphoreGive( xMutex_i2c );

  uint8_t avg_sample = 10;
  int32_t prs_arr[avg_sample + 5];
  int32_t prs_arr_sort[avg_sample + 5];
  int32_t temp;
  uint8_t count = 0;

  uint32_t time_bmp180_update = 0;
  uint32_t time_start = 0;
  uint32_t time_i2c_run = 0;
  uint32_t time_freetime = 0;
  uint32_t time_1sinterval = 0;
  int count_1s = 0;
  int count_per_second = 0;
  static uint32_t time_100ms_interval = 0;

  TickType_t xLastWakeTime;
  const TickType_t xElapse = 50;//10hz loop
  xLastWakeTime = xTaskGetTickCount();
  for (;;) {
    // Wait for the next cycle.
    vTaskDelayUntil( &xLastWakeTime, xElapse );

    xSemaphoreTake( xMutex_i2c, portMAX_DELAY );
    pressure = bmp.readPressure();
    xSemaphoreGive( xMutex_i2c );


    prs_arr[count] = pressure;
    if (++count >= avg_sample)count = 0;

    //transfer to new variable
    for (int i = 0; i < avg_sample; i++) {
      prs_arr_sort[i] = prs_arr[i];
    }

    //sorting - ASCENDING ORDER
    for (int i = 0; i < avg_sample; i++) {
      for (int j = i + 1; j < avg_sample; j++) {
        if (prs_arr_sort[i] > prs_arr_sort[j]) {
          temp  = prs_arr_sort[i];
          prs_arr_sort[i] = prs_arr_sort[j];
          prs_arr_sort[j] = temp;
        }
      }
    }

    // averaging
    uint32_t prs_sum = 0;
    for (uint8_t i = 3; i <= 6; i++) {
      prs_sum += prs_arr_sort[i];
    }
    xSemaphoreTake( xMutex_pressData, portMAX_DELAY );
    filteredPressure = prs_sum / 4;
    xSemaphoreGive( xMutex_pressData );
    
    if (debug_bmp) {
      sprintf(buffer, "filtpress,%d\n",
              filteredPressure );
      //      sprintf(buffer, "press,%d,filtpress,%d\n",
      //              pressure, filteredPressure );
      printBothSerialTelnet(buffer, strlen(buffer));
    }



    if (OTA_UPLOAD_RUN == true) {
      vTaskDelete( NULL );// delete this task
    }
  }
}
