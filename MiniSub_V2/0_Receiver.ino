
volatile uint32_t Channel_RAW[12];
volatile uint32_t last_ISR = 0;
volatile uint32_t last_complete_ppm_interval = 0;
volatile uint32_t complete_ppm_interval = 0;
volatile uint8_t counter = 0;
volatile uint8_t max_counter = 0;
volatile uint8_t max_channel = 10;
volatile bool new_ppm = false;

portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

//The minimum value (time) after which the signal frame is considered to
//be finished and we can start to expect a new signal frame.
const uint32_t minTime = 900;
const uint32_t maxTime = 2100;
const uint32_t blankTime = 2100;

// ISR Interrupt
void IRAM_ATTR handleInterrupt() {
  portENTER_CRITICAL_ISR(&mux);

  uint32_t ISR_now = micros();
  uint32_t ISR_interval = ISR_now - last_ISR;
  last_ISR = ISR_now;

  if (ISR_interval > blankTime) {//interval more than 2.5ms reset counter
    counter = 0;
    complete_ppm_interval = ISR_now - last_complete_ppm_interval;
    last_complete_ppm_interval = ISR_now;
    new_ppm = true;
  } else {
    //check if new data is in range
    if (ISR_interval < maxTime && ISR_interval > minTime) {
      Channel_RAW[counter] = (uint16_t)ISR_interval;
      counter++;
    }
    if (max_counter < counter)max_counter = counter;
    if (max_channel < counter)counter = max_channel;
  }

  portEXIT_CRITICAL_ISR(&mux);
}

void Task_9(void *pvParameters) { // "Task Receiver"
  (void) pvParameters;

  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.print(" Receiver");
  xSemaphoreGive( xMutex_serial );
  vTaskDelay(500);
  xSemaphoreTake( xMutex_serial, portMAX_DELAY );
  Serial.println();
  xSemaphoreGive( xMutex_serial );

  const uint8_t interruptPin = 35;
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, FALLING);

  TickType_t xLastWakeTime;
  const TickType_t xElapse = 10;//100Hz Loop
  xLastWakeTime = xTaskGetTickCount();
  for (;;) {
    // Wait for the next cycle.
    vTaskDelayUntil( &xLastWakeTime, xElapse );

    if (new_ppm) {
      portENTER_CRITICAL(&mux);
      new_ppm = false;
      for (uint8_t i = 0; i < max_counter; i++) {
        Channel[i] = (Channel[i] + Channel_RAW[i]) / 2;
        //        Channel[i] = Channel_RAW[i];
      }
      portEXIT_CRITICAL(&mux);


      if (debug_ppm) {
        //Serial.printf(" %d,", complete_ppm_interval);
        strcpy(buffer, "");
        char stemp[10];
        for (uint8_t i = 0; i < max_counter; i++) {
          sprintf(stemp, "%d, ", Channel[i]);
          strcat(buffer, stemp);
        }
        strcat(buffer, "\n");
        printBothSerialTelnet(buffer, strlen(buffer));
      }
    }

    if (OTA_UPLOAD_RUN == true) {
      detachInterrupt(digitalPinToInterrupt(interruptPin));
      vTaskDelete( NULL );
    }
  }
}
