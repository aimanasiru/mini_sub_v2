#define OTA_UPLOAD //for enable wifi Flash upload.
//#define ENABLE_OLED //for enable Oled Function
#define MAX_SRV_CLIENTS 3
#define Total_Motor 7

#include "EEPROM.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>

#include <ESPmDNS.h>
#include <Update.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>
#ifdef ENABLE_OLED
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#endif
#include "MPU9250.h"
#include <MadgwickAHRS.h>
#include <FastPID.h>

//==========  Serial Debug boolean ==========//
volatile bool OTA_UPLOAD_RUN = false;
bool calIMU = false;
bool newPID = false;
bool isArmed = false;
bool isPID = false;
bool debug_pid_rpy  = false;
bool debug_bmp      = false;
bool debug_ppm      = false;
bool debug_bmp180   = false;
bool debug_oled     = false;
bool debug_servo    = false;
bool debug_motor    = false;
bool debug_process_control = false;
bool debug_ledBlink = false;
bool debug_mpu9250_0 = false;
bool debug_mpu9250_1 = false;
bool debug_mpu9250_2 = false;
bool debug_yaw = false;
bool debug_hea = false;
bool servoSweep_test = false;
bool directChannel_test = false;
bool processFreq_test = false;
bool auto_level = true;                 //Auto level on (true) or off (false).
//boolean isPower = false, last_isPower = false;

//=========== Global Variable ===========//
uint16_t Channel[12];
uint16_t Servo_Out[Total_Motor];
float ROLL, PITCH, YAW;
char buffer[400];

struct EEPROM_Object {
  float Kp[7];
  float Ki[7];
  float Kd[7];
  float acCal[3];
  float gyCal[3];
} saved_data;

//==========    WIFI Constant   ============//
const char* host = "minisubv2";
const char* ssid = "Delimawati@unifi";
const char* password = "bnf3559myvibiru";
//const char* ssid = "neos";
//const char* password = "12345678";

//========    Class Declaration   ==========//
WebServer server(80);
WiFiServer server2(23);
WiFiClient serverClients[MAX_SRV_CLIENTS];
Adafruit_BMP085 bmp;


//========    RTOS Declaration   ==========//
SemaphoreHandle_t xMutex_i2c;     //To Control I2C resource
SemaphoreHandle_t xMutex_serial;  //To Control Serial Resource
SemaphoreHandle_t xMutex_spi;     //To Control SPI Resource
SemaphoreHandle_t xMutex_telnet;  //To Control telnet Resource
SemaphoreHandle_t xMutex_imuData;  //To Control imu Resource
SemaphoreHandle_t xMutex_pressData;  //To Control pressure Resource

TaskHandle_t Task1;
TaskHandle_t Task2;
TaskHandle_t Task4;
TaskHandle_t Task5;
TaskHandle_t Task6;
TaskHandle_t Task8;
TaskHandle_t Task9;
TaskHandle_t Task10;
TaskHandle_t Task11;
TaskHandle_t Task12;

void setup(void) {

  Wire.begin();           // Wire must be started first
  Wire.setClock(400000);  // Supported baud rates are 100kHz, 400kHz, and 1000kHz

  Serial.begin(9600);
  Serial.println("\nMiniSubVersion-2.0.0");


  getEEPROM();

  Serial.print("\nTask:");


  //   vTaskDelay(1);       // delay in millis;
  //  xTaskCreatePinnedToCore(
  //    Task_1,               /* Task function. */
  //    "Task Blink LED",     /* name of task. */
  //    1024,                 /* Stack size of task */
  //    NULL,                 /* parameter of the task */
  //    1,                    /* priority of the task */
  //    &Task1,               /* Task handle to keep track of created task */
  //    0);                   /* pin task to core 0 */

  xMutex_i2c = xSemaphoreCreateMutex();
  xMutex_serial = xSemaphoreCreateMutex();
  xMutex_spi = xSemaphoreCreateMutex();
  xMutex_telnet = xSemaphoreCreateMutex();
  xMutex_imuData = xSemaphoreCreateMutex();
  xMutex_pressData = xSemaphoreCreateMutex();

  //=======================   TASK CORE 0     ===========================//
  xTaskCreatePinnedToCore(Task_1, "Blink LED"       , 1024, NULL, 1, &Task1, 0);
#ifdef OTA_UPLOAD
  xTaskCreatePinnedToCore(Task_2, "OTA_update"      , 4096, NULL, 2, &Task2, 0);
#endif
  xTaskCreatePinnedToCore(Task_6, "SerialRead"      , 4096, NULL, 2, &Task6, 0);
  xTaskCreatePinnedToCore(Task_8, "Servo"           , 2048, NULL, 4, &Task8, 0);
  xTaskCreatePinnedToCore(Task_10, "ProcessControl" , 4096, NULL, 5, &Task10, 0);
  xTaskCreatePinnedToCore(Task_11, "TelnetSerial"   , 4096, NULL, 2, &Task11, 0);

  //========================    TASK CORE 1   ==========================//
#ifdef ENABLE_OLED
  xTaskCreatePinnedToCore(Task_4, "OLED I2C"      , 2048, NULL, 1, &Task4, 1);
#endif
  xTaskCreatePinnedToCore(Task_5, "BMP180 I2C"    , 4096, NULL, 2, &Task5, 1);
  xTaskCreatePinnedToCore(Task_9, "RECEIVER"      , 2048, NULL, 4, &Task9, 1);
  xTaskCreatePinnedToCore(Task_12, "MPU9250"      , 8096, NULL, 6, &Task12, 1);

}

void loop(void) {}

void getEEPROM() {
  if (!EEPROM.begin(1000)) {
    Serial.println("failed to initialise EEPROM");
  }
  EEPROM.get(0, saved_data);

  char tbuf[50];
  strcpy(buffer, "");
  for (uint8_t i = 0; i < 6; i++) {
    sprintf(tbuf, "PID%d %.2f, %.2f, %.2f,\n" , i,
            saved_data.Kp[i], saved_data.Ki[i], saved_data.Kd[i]);
    strcat(buffer, tbuf);
  }
  sprintf(tbuf, "Acc : %.2f, %.2f, %.2f, Gyro:%.2f, %.2f, %.2f \n" ,
          saved_data.acCal[0], saved_data.acCal[1], saved_data.acCal[2],
          saved_data.gyCal[0], saved_data.gyCal[1], saved_data.gyCal[2]
         );
  strcat(buffer, tbuf);
  Serial.print(buffer);
}

void saveEEPROM() {
  EEPROM.put(0, saved_data);
  EEPROM.commit();
}
