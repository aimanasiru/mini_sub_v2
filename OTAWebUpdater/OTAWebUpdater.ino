#define LED_BUILTIN 2

#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>

const char* host = "esp32";
const char* ssid = "Delimawati@unifi";
const char* password = "bnf3559myvibiru";

WebServer server(80);

void Task_1( void *pvParameters );
void Task_2( void *pvParameters );

void setup(void) {
  xTaskCreatePinnedToCore(Task_1, "Task1"           , 1024, NULL, 1, NULL, 0);
  xTaskCreatePinnedToCore(Task_2, "Task_OTA_update" , 4096, NULL, 0, NULL, 0);
}

void loop(void) {}


void Task_1(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  pinMode(LED_BUILTIN, OUTPUT);

  for (;;) {
    digitalWrite(LED_BUILTIN, HIGH);
    vTaskDelay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    vTaskDelay(1000);
  }
}
